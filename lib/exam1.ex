defmodule Exam1 do
  def median(list) do
    l= length(list)
    m= length(list)/2
    if(rem(l,2)==1) do
    Enum.at(list, ceil(m)-1)
    else
      (Enum.at(list, ceil(m)-1)+ Enum.at(list, ceil(m)))/2
    end

  end
  @spec quartile1(any) :: nil
  def quartile1(list) do
    m= length(list)/2
    median(Enum.slice(list, 0..floor(m)-1))

  end
  def quartile3(list) do
    m= length(list)/2
    l=length(list)
    median(Enum.slice(list, floor(m)..l-1))

  end

  def iqr(list) do
    quartile3(list)- quartile1(list)
  end
end
